.. Jefferson Research Resources documentation master file, created by
   sphinx-quickstart on Sun Apr  4 18:18:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:hide-toc:

####################################
Jefferson Enterprise Data Resources
####################################
   
.. toctree::
   :hidden:

This site aims to be a comprehensive, centralized catalog of data tools used by researchers and clinicians across Jefferson Health. By maintaining an inventory of available resources, the Jefferson data community can better ascertain the tool coverage for our needs and connect users with the relevant owner.

.. admonition:: Contact `skcc.informatics@jefferson.edu`_ for any of the following:
   :class: note

   - inform us about a resource that is missing from the site
   - propose the development or purchase of new data tools
   - express a data-related need
   - ask questions related to data resources, tools, or initiatives
   - suggest ideas for fostering an engaged data community 

.. _skcc.informatics@jefferson.edu: skcc.informatics@jefferson.edu

---------

Data Resource Overview
=======================

+----------------------------------+-------------------------------------------------------+----------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Resource                         | Owner                                                 | Access                                       | Capabilities                                                                                                                                                         |
+==================================+=======================================================+==============================================+======================================================================================================================================================================+
| **REDCap**                       | Cancer Informatics (REDCap@jefferson.edu)             | Automated off REDCap homepage                | Data capture, survey distribution, eConsent                                                                                                                          |
+----------------------------------+-------------------------------------------------------+----------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **Qualtrics**                    | DICE (contact TBD)                                    | Automated via surveys.jefferson.edu          | Survey                                                                                                                                                               |
+----------------------------------+-------------------------------------------------------+----------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **OpenSpecimen**                 | Cancer Informatics (skcc.informatics@jefferson.edu)   | Limited access, email for more info          | Specimen tracking, annotation, and reporting                                                                                                                         |
+----------------------------------+-------------------------------------------------------+----------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **I2B2**                         | Cancer Informatics (skcc.informatics@jefferson.edu)   | Email to request access                      | De-ID cohort generation                                                                                                                                              |
+----------------------------------+-------------------------------------------------------+----------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **TriNetX**                      | Cancer Informatics (skcc.informatics@jefferson.edu)   | Email to request access                      | De-ID cohort generation, analytics, cohort comparison, multi-institutional data integration                                                                          |
+----------------------------------+-------------------------------------------------------+----------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **Genomic Research Data Portal** | Cancer Informatics (skcc.informatics@jefferson.edu)   | Email to request access                      | Jefferson-centric genomic data with layered clinical data for research use                                                                                           |
+----------------------------------+-------------------------------------------------------+----------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **Epic SlicerDicer**             | Enterprise Analytics                                  | ServiceNow request after gaining Epic access | Quickly explore large quantities of data; Show data to patients to encourage changes in behavior; Location; Find patients for research studies or screening programs |
+----------------------------------+-------------------------------------------------------+----------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **Patient360 Data Mart**         | Enterprise Analytics                                  | ServiceNow request                           | Enterprise view of all Jefferson patient information Dec 2019-current                                                                                                |
+----------------------------------+-------------------------------------------------------+----------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+